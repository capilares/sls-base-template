import { DEFAULT_ERRORS } from "../configs/defaultProperties";
export const createErrorResponse = (errorObject, thrownError, message) => {

    console.error('Error: ', thrownError);

    const { statusCode, headers, body } = errorObject;
    if (message) {
        body.error.message = message;
    }

    return {
        statusCode,
        headers,
        body: JSON.stringify(body)
    };
};

export const createInvokeErrorResponse = (errorObject, thrownError, message) => {

    console.error('Error: ', thrownError);

    const { statusCode, headers, body } = errorObject;
    if (message) {
        body.error.message = message;
    }

    return JSON.stringify({
        statusCode,
        headers,
        body
    });
};

export const createErrorResponseFromExternalFunction = (thrownError) => {

    const { errorMessage } = JSON.parse(thrownError);

    if (errorMessage.includes("Task timed out after")) {
        return createErrorResponse(DEFAULT_ERRORS.DefaultError, thrownError);
    }

    console.error('Error: ', errorMessage);

    const { statusCode, headers, body } = JSON.parse(errorMessage);

    return {
        statusCode,
        headers,
        body: JSON.stringify(body)
    };
};

export const createSuccessResponse = (body) => {

    return {
        statusCode: 200,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
    };
};