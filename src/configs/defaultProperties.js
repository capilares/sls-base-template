export const SERVICE_CODE = '2';
export const SERVICE_NAME = 'Auth Service';

// ERRORS
export const DEFAULT_ERRORS = {
    DefaultError: {
        statusCode: 500,
        headers: { 'Content-Type': 'application/json' },
        body: {
            error: {
                code: SERVICE_CODE.concat('001'),
                name: 'DefaultError',
                message: 'Something went wrong',
            }
        }
    },
};