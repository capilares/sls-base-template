async function hello(event, context) {
  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'Hello that\'s me! Good luck with this new service' }),
  };
}

export const handler = hello;


