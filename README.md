# ProjectCodeNameCapilares: Base Serverless Framework Template

> Base Serverless Frameword Template

## What's included
* Folder structure used consistently across our projects.
* [serverless-pseudo-parameters plugin](https://www.npmjs.com/package/serverless-pseudo-parameters): Allows you to take advantage of CloudFormation Pseudo Parameters.
* [serverless-bundle plugin](https://www.npmjs.com/package/serverless-pseudo-parameters): Bundler based on the serverless-webpack plugin - requires zero configuration and fully compatible with ES6/ES7 features.

## Getting started
```
sls create --name YOUR_PROJECT_NAME --template-url https://bitbucket.org/capilares/sls-base-template
cd YOUR_PROJECT_NAME
npm install
```

You are ready to go!

> For readme.md file delete the above and change bellow

# Service Name

## Requirements
* node.js
* npm
* serverless CLI
* AWS CLI

## Deploy for development stage
```
Put here the commands / steps
```

## Doploy for testing stage
```
Put here the commands / steps
```

## Deploy for production stage
```
Put here the commands / steps
```
